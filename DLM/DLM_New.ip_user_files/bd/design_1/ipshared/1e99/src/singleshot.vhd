
library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

entity singleshot is
port (
Trigger : in std_logic;
Clock : in std_logic;
Pulse : out std_logic
);
end singleshot;

architecture impl of singleshot is

signal QA: std_logic := '0';
signal QB: std_logic := '0';

begin

Pulse <= QB;

process (Trigger, QB)
    begin
        if QB='1' then
            QA <= '0';
        elsif (Trigger'event and Trigger='1') then
            QA <= '1';
    end if;
end process;

process (Clock)
    begin
        if Clock'event and Clock ='1' then
            QB <= QA;
        end if;
    end process;

end impl;