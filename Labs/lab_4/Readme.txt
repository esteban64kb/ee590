
********************************
* Instructions to run Lab_4 *
********************************

- Download Lab_4 project from https://bitbucket.org/esteban64kb/ee590/src/master/Labs/lab_4/

- Copy folder PYNQ-master to C:\

- Open Vivado project (located at: C:\PYNQ-master\boards\Pynq-Z2\base\base

- Open TCL console and enter the next commands:

     1) cd C:/PYNQ-master/boards/Pynq-Z2/base
     2) source base.tcl

        	- If getting next error

					ERROR: [BD_TCL-114] Design <base> already exists in your project, please set the variable <design_name> to another value.

				Open Block Design and delete everything (complete Base Overlay). Then repeat steps 1 and 2 in the TCL console.

If that doesn't work please create a new project following the instructions from slides Lab_4. Please let me know if you have any questions.

If it did work you should have a new base.bit file at the end of the process.

Esteban Moreno



