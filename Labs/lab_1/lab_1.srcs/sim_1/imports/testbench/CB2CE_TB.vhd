-- Engineer: Fearghal Morgan
-- viciLogic 
-- Create Date: 07/14/2012 
-- Testbench
-- CB2CE 2-bit up counter with asynchronous rst and clock enable input

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- consider removing or replacing with IEEE.NUMERIC_STD.ALL;

entity CB2CE_TB is end CB2CE_TB; -- testbench has no inputs or outputs

architecture behaviour OF CB2CE_TB IS 

component CB2CE is 	-- declare the component
    Port ( clk 		: in STD_LOGIC;
           rst 		: in STD_LOGIC;           
           ce 		: in STD_LOGIC;           
           count	: out STD_LOGIC_VECTOR (1 downto 0);
		   TC 		: out STD_LOGIC;
		   ceo 		: out STD_LOGIC
          );
end component;

-- Define testbench signals which connect to/from the component
-- Typically use the same signals as the component, though not necessarily.
signal clk   : std_logic := '1';  -- std_logic signal type can have values 'U', 'X', '0', '1', etc 
								  -- so assign initial clk logic level to '1' in order that toggling clk will give 0 or 1 (and not U).
signal rst   : std_logic;
signal ce    : std_logic; 
signal count : std_logic_vector(1 downto 0); 
signal TC    : std_logic; 
signal ceo    : std_logic; 

-- simulation parameters
constant period : time := 20 ns;	-- 50MHz clk
signal endOfSim : boolean := false; -- useful flag

begin

-- component to be tested
CB2CE_i: CB2CE port map
      (clk   => clk,
 	   rst   => rst, 
 	   ce    => ce,
   	   count => count,
   	   TC    => TC,
   	   ceo   => ceo
	  ); 

-- clk stimulus continuing until simulation stimulus is no longer applied
clkStim : process (clk)
begin
  if endOfSim = false then
     clk <= not clk after period/2;
  end if;
end process;

stim: process 	-- apply rst and ce stimulus
begin 
    report "Test 1: Assign inputs, assert/deassert rst"; -- message to simulation transcript
						 -- Expected o/ps: all 0
	ce  <= '0';   		 -- deassert, count disabled 
	rst <= '1';  		 -- assert rst
	wait for 1.2*period; -- simulate 0.2*period past the second clock rising (active) edge to align the application of input signal changes away from active clk edge.
						 -- 0.5*period to rising clk edge, 0.5*period to falling clk edge, 0.5*period to rising clk edge, 0.2*period to this point 
	rst <= '0';	 		 -- de-assert rst
	wait for 2*period;   -- wait for 2 clock steps 
	
    
	report "Test 2: assert ce and wait for 5 clock periods to exercise the counter";
						 -- Expected o/p: count 1,2,3   TC asserted when count is 3, and ceo asserted when ce and TC are asserted.
	ce <= '1';	  
	wait for 3*period;	

	report "Test 3: deassert ce and wait for 2 clock periods";
						 -- Expected o/p: counter value remains unchanged, ceo deasserted, TC remains asserted.
	ce <= '0';	  
	wait for 2*period;	

	report "Test 4: assert ce and wait for 2 clock periods";
						 -- Expected o/p: count 0, 1 
	ce <= '1';	  
	wait for 2*period;	
	
	report "Test 5: assert rst";
	wait for 0.5*period; -- wait until low phase of clk signal	
	rst <= '1';  		 -- assert rst
	wait for 1.5*period; -- counter clears asynchronously (immediately on assertion of rst) 

    rst <= '0';  		 -- deassert rst
    wait for period;  

	endOfSIm <= true;    -- assert endOfSim flag
    report "simulation done"; -- message to simulation transcript

    wait; 				-- wait forever. Otherwise stimulus will be re-applied (forever) since no stim process sensitivity list exists
END PROCESS;

END;