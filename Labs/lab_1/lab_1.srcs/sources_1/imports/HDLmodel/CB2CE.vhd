-- Engineer: Fearghal Morgan
-- viciLogic 
-- Create Date: 07/14/2012 
-- CB2CE 2-bit up counter with 
-- 	Asynchronous rst and chip enable input
-- 	Outputs: count value, terminal count (TC) and clock enable out (ceo)

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity CB2CE is
    Port ( clk 		: in STD_LOGIC;
           rst 		: in STD_LOGIC;           
           ce 		: in STD_LOGIC;           
           count	: out STD_LOGIC_VECTOR (1 downto 0);
		   TC 		: out STD_LOGIC;
		   ceo 		: out STD_LOGIC
          );
end CB2CE;

architecture RTL of CB2CE is
signal NS : STD_LOGIC_VECTOR(1 downto 0); -- next state signal
signal CS : STD_LOGIC_VECTOR(1 downto 0); -- current state signal
signal intTC : STD_LOGIC;                 

begin

-- ========= NSAndOPDecode start
NSDecode_i: process (CS, ce)
begin
    NS <= CS;         -- default assignment
	if ce = '1' then 
		NS <= CS + 1; -- increment
	end if;
end process;

-- OPDecode_i logic. Use intTC internal signal since 
-- cannot use TC on right hand side of ceo assignment 
OPDecode_TC_i: process (CS)
begin
	intTC <= '0'; -- default
	if CS = "11" then 
		intTC <= '1'; 
	end if;
end process;
OPDecode_asgnTC_i: TC  <= intTC; 

OPDecode_ceo_i:    ceo <= ce and intTC;
-- ========= NSAndOPDecode end


stateReg_i: process(clk, rst)
begin
	if rst = '1' then
		CS <= (others => '0');
	elsif clk'event and clk = '1' then
		CS <= NS;
	end if;
end process;

asgnCount_i: count <= CS; 

end RTL;