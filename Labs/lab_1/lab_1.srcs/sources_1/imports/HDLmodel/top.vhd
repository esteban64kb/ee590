----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/27/2016 01:26:08 PM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( clk 		: in STD_LOGIC;       
           btns 	: in STD_LOGIC_VECTOR (3 downto 0);           
           leds	    : out STD_LOGIC_VECTOR (3 downto 0)
          );
end top;

architecture Behavioral of top is


component CB2CE is
    Port ( clk 		: in STD_LOGIC;
           rst 		: in STD_LOGIC;           
           ce 		: in STD_LOGIC;           
           count	: out STD_LOGIC_VECTOR (1 downto 0);
		   TC 		: out STD_LOGIC;
		   ceo 		: out STD_LOGIC
          );
end component;

component singleshot is
port (
    Trigger : in std_logic;
    Clock : in std_logic;
    Pulse : out std_logic
    );
end component;

signal ce_pulse : std_logic := '0';


begin


singleshot_uut : singleshot
port map (
    Trigger => btns(1),
    Clock => clk,
    Pulse => ce_pulse
    );


CB2CE_uut : CB2CE
port map (
clk => clk,
rst => btns(0),
ce => ce_pulse,
count => leds(3 downto 2),
TC => leds(1),
ceo => leds(0)
);

end Behavioral;
