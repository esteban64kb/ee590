library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DLM_core is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Width of ID for for write address, write data, read address and read data
		C_S_AXI_ID_WIDTH	: integer	:= 12;
		-- Width of S_AXI data bus
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		-- Width of S_AXI address bus
		C_S_AXI_ADDR_WIDTH	: integer	:= 11;
		-- Width of optional user defined signal in write address channel
		C_S_AXI_AWUSER_WIDTH	: integer	:= 0;
		-- Width of optional user defined signal in read address channel
		C_S_AXI_ARUSER_WIDTH	: integer	:= 0;
		-- Width of optional user defined signal in write data channel
		C_S_AXI_WUSER_WIDTH	: integer	:= 0;
		-- Width of optional user defined signal in read data channel
		C_S_AXI_RUSER_WIDTH	: integer	:= 0;
		-- Width of optional user defined signal in write response channel
		C_S_AXI_BUSER_WIDTH	: integer	:= 0
	);
	port (
		-- Users to add ports here
		
		-- Register for inputs to User Design
		variableInMemory : out std_logic_vector(511 downto 0);
		-- Register for outputs from User Design
		variableOutMemory : in std_logic_vector(4095 downto 0);
		-- User Clock signal
		VL_UserClk : out std_logic;
		-- User Reset signal
		VL_UserRst : out std_logic;
		
		-- ports for BRAM
		--BRAM0
		-- BRAM0 Address for read/write
		BRAM0ADDRESS : in Std_logic_vector(13 downto 0);
		-- BRAM0 32bit data for write
		BRAM0WDATA 	: in Std_logic_vector(31 downto 0);
		-- BRAM0 write enable
		BRAM0WRITE 	: in Std_logic;
		-- BRAM0 read enable
		BRAM0READ 	: in Std_logic; --do we need custom read signal handling?
		-- BRAM0 32bit read data
		BRAM0RDATA 	: out Std_logic_vector(31 downto 0);
		--BRAM1
		-- BRAM1 Address for read/write
		BRAM1ADDRESS : in Std_logic_vector(13 downto 0);
		-- BRAM1 32bit data for write
		BRAM1WDATA 	: in Std_logic_vector(31 downto 0);
		-- BRAM1 write enable
		BRAM1WRITE 	: in Std_logic;
		-- BRAM1 read enable
		BRAM1READ 	: in Std_logic; --do we need custom read signal handling?
		-- BRAM1 32bit read data
		BRAM1RDATA 	: out Std_logic_vector(31 downto 0);
		
		-- ports for 6x LEDs (2x RGB 4x general)
		DISPLAYLED 		: IN Std_logic_vector(3 downto 0);
		EXTDISPLAYLED 	: OUT Std_logic_vector(3 downto 0);
		-- Should use PWM for tri-color/RGB LEDs, max 50% duty cycle
		RGBLED 		: IN Std_logic_vector(5 downto 0); 
		EXTRGBLED 	: OUT Std_logic_vector(5 downto 0); 
		-- ports for 2x Switches
		EXTSWITCHES : IN std_logic_vector(1 downto 0);
		SWITCHES 	: OUT std_logic_vector(1 downto 0);
		-- ports for 4x buttons
		EXTBUTTONS	: IN Std_logic_vector(3 downto 0);
		BUTTONS 	: OUT Std_logic_vector(3 downto 0);
		-- ports for Memory DMA controller
		-- ports for GPIO
		
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global Clock Signal
		S_AXI_ACLK	: in std_logic;
		-- Global Reset Signal. This Signal is Active LOW
		S_AXI_ARESETN	: in std_logic;
		-- Write Address ID
		S_AXI_AWID	: in std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		-- Write address
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Burst length. The burst length gives the exact number of transfers in a burst
		S_AXI_AWLEN	: in std_logic_vector(7 downto 0);
		-- Burst size. This signal indicates the size of each transfer in the burst
		S_AXI_AWSIZE	: in std_logic_vector(2 downto 0);
		-- Burst type. The burst type and the size information, 
    -- determine how the address for each transfer within the burst is calculated.
		S_AXI_AWBURST	: in std_logic_vector(1 downto 0);
		-- Lock type. Provides additional information about the
    -- atomic characteristics of the transfer.
		S_AXI_AWLOCK	: in std_logic;
		-- Memory type. This signal indicates how transactions
    -- are required to progress through a system.
		S_AXI_AWCACHE	: in std_logic_vector(3 downto 0);
		-- Protection type. This signal indicates the privilege
    -- and security level of the transaction, and whether
    -- the transaction is a data access or an instruction access.
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		-- Quality of Service, QoS identifier sent for each
    -- write transaction.
		S_AXI_AWQOS	: in std_logic_vector(3 downto 0);
		-- Region identifier. Permits a single physical interface
    -- on a slave to be used for multiple logical interfaces.
		S_AXI_AWREGION	: in std_logic_vector(3 downto 0);
		-- Optional User-defined signal in the write address channel.
		--S_AXI_AWUSER	: in std_logic_vector(C_S_AXI_AWUSER_WIDTH-1 downto 0);
		-- Write address valid. This signal indicates that
    -- the channel is signaling valid write address and
    -- control information.
		S_AXI_AWVALID	: in std_logic;
		-- Write address ready. This signal indicates that
    -- the slave is ready to accept an address and associated
    -- control signals.
		S_AXI_AWREADY	: out std_logic;
		-- Write Data
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Write strobes. This signal indicates which byte
    -- lanes hold valid data. There is one write strobe
    -- bit for each eight bits of the write data bus.
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		-- Write last. This signal indicates the last transfer
    -- in a write burst.
		S_AXI_WLAST	: in std_logic;
		-- Optional User-defined signal in the write data channel.
		--S_AXI_WUSER	: in std_logic_vector(C_S_AXI_WUSER_WIDTH-1 downto 0);
		-- Write valid. This signal indicates that valid write
    -- data and strobes are available.
		S_AXI_WVALID	: in std_logic;
		-- Write ready. This signal indicates that the slave
    -- can accept the write data.
		S_AXI_WREADY	: out std_logic;
		-- Response ID tag. This signal is the ID tag of the
    -- write response.
		S_AXI_BID	: out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		-- Write response. This signal indicates the status
    -- of the write transaction.
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		-- Optional User-defined signal in the write response channel.
		--S_AXI_BUSER	: out std_logic_vector(C_S_AXI_BUSER_WIDTH-1 downto 0);
		-- Write response valid. This signal indicates that the
    -- channel is signaling a valid write response.
		S_AXI_BVALID	: out std_logic;
		-- Response ready. This signal indicates that the master
    -- can accept a write response.
		S_AXI_BREADY	: in std_logic;
		-- Read address ID. This signal is the identification
    -- tag for the read address group of signals.
		S_AXI_ARID	: in std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		-- Read address. This signal indicates the initial
    -- address of a read burst transaction.
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Burst length. The burst length gives the exact number of transfers in a burst
		S_AXI_ARLEN	: in std_logic_vector(7 downto 0);
		-- Burst size. This signal indicates the size of each transfer in the burst
		S_AXI_ARSIZE	: in std_logic_vector(2 downto 0);
		-- Burst type. The burst type and the size information, 
    -- determine how the address for each transfer within the burst is calculated.
		S_AXI_ARBURST	: in std_logic_vector(1 downto 0);
		-- Lock type. Provides additional information about the
    -- atomic characteristics of the transfer.
		S_AXI_ARLOCK	: in std_logic;
		-- Memory type. This signal indicates how transactions
    -- are required to progress through a system.
		S_AXI_ARCACHE	: in std_logic_vector(3 downto 0);
		-- Protection type. This signal indicates the privilege
    -- and security level of the transaction, and whether
    -- the transaction is a data access or an instruction access.
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		-- Quality of Service, QoS identifier sent for each
    -- read transaction.
		S_AXI_ARQOS	: in std_logic_vector(3 downto 0);
		-- Region identifier. Permits a single physical interface
    -- on a slave to be used for multiple logical interfaces.
		S_AXI_ARREGION	: in std_logic_vector(3 downto 0);
		-- Optional User-defined signal in the read address channel.
		--S_AXI_ARUSER	: in std_logic_vector(C_S_AXI_ARUSER_WIDTH-1 downto 0);
		-- Write address valid. This signal indicates that
    -- the channel is signaling valid read address and
    -- control information.
		S_AXI_ARVALID	: in std_logic;
		-- Read address ready. This signal indicates that
    -- the slave is ready to accept an address and associated
    -- control signals.
		S_AXI_ARREADY	: out std_logic;
		-- Read ID tag. This signal is the identification tag
    -- for the read data group of signals generated by the slave.
		S_AXI_RID	: out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		-- Read Data
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Read response. This signal indicates the status of
    -- the read transfer.
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		-- Read last. This signal indicates the last transfer
    -- in a read burst.
		S_AXI_RLAST	: out std_logic;
		-- Optional User-defined signal in the read address channel.
		--S_AXI_RUSER	: out std_logic_vector(C_S_AXI_RUSER_WIDTH-1 downto 0);
		-- Read valid. This signal indicates that the channel
    -- is signaling the required read data.
		S_AXI_RVALID	: out std_logic;
		-- Read ready. This signal indicates that the master can
    -- accept the read data and response information.
		S_AXI_RREADY	: in std_logic
	);
end DLM_core;

architecture arch_imp of DLM_core is

	-- AXI4FULL signals
	signal axi_awaddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_awready	: std_logic;
	signal axi_wready	: std_logic;
	signal axi_bresp	: std_logic_vector(1 downto 0);
	signal axi_buser	: std_logic_vector(C_S_AXI_BUSER_WIDTH-1 downto 0);
	signal axi_bvalid	: std_logic;
	signal axi_araddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_arready	: std_logic;
	signal axi_rdata	: std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal axi_rresp	: std_logic_vector(1 downto 0);
	signal axi_rlast	: std_logic;
	signal axi_ruser	: std_logic_vector(C_S_AXI_RUSER_WIDTH-1 downto 0);
	signal axi_rvalid	: std_logic;
	-- aw_wrap_en determines wrap boundary and enables wrapping
	signal  aw_wrap_en : std_logic; 
	-- ar_wrap_en determines wrap boundary and enables wrapping
	signal  ar_wrap_en : std_logic;
	-- aw_wrap_size is the size of the write transfer, the
	-- write address wraps to a lower address if upper address
	-- limit is reached
	signal aw_wrap_size : integer;
	-- ar_wrap_size is the size of the read transfer, the
	-- read address wraps to a lower address if upper address
	-- limit is reached
	signal ar_wrap_size : integer;
	-- The axi_awv_awr_flag flag marks the presence of write address valid
	signal axi_awv_awr_flag    : std_logic;
	--The axi_arv_arr_flag flag marks the presence of read address valid
	signal axi_arv_arr_flag    : std_logic;
	-- The axi_awlen_cntr internal write address counter to keep track of beats in a burst transaction
	signal axi_awlen_cntr      : std_logic_vector(7 downto 0);
	--The axi_arlen_cntr internal read address counter to keep track of beats in a burst transaction
	signal axi_arlen_cntr      : std_logic_vector(7 downto 0);
	signal axi_arburst      : std_logic_vector(2-1 downto 0);
	signal axi_awburst      : std_logic_vector(2-1 downto 0);
	signal axi_arlen      : std_logic_vector(8-1 downto 0);
	signal axi_awlen      : std_logic_vector(8-1 downto 0);
	--local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	--ADDR_LSB is used for addressing 32/64 bit registers/memories
	--ADDR_LSB = 2 for 32 bits (n downto 2) 
	--ADDR_LSB = 3 for 42 bits (n downto 3)

	constant ADDR_LSB  : integer := (C_S_AXI_DATA_WIDTH/32)+ 1;
	constant OPT_MEM_ADDR_BITS : integer := 8; --2**8 = 255 words of addressable memory
	constant USER_NUM_MEM: integer := 1;
	constant low : std_logic_vector (C_S_AXI_ADDR_WIDTH - 1 downto 0) := (others => '0');
	------------------------------------------------
	---- Signals for user logic memory space example
	--------------------------------------------------
	signal mem_address : std_logic_vector(OPT_MEM_ADDR_BITS downto 0);	-- Word addressable memory (AXI addresses are byte addresses by default)
	signal mem_data_out : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);

	signal int_variableInMemory : std_logic_vector(1023 downto 0); 	--512 inputs plus additional space for other signals
    signal int_variableOutMemory : std_logic_vector(8191 downto 0); --4K outputs plus additional space for other signals
	
	--BYTE RAM SIGNALS
    signal mem_rden : std_logic;
    signal mem_wren : std_logic;
    signal data_in0  : std_logic_vector(8-1 downto 0);
    signal data_out0 : std_logic_vector(8-1 downto 0);
    signal data_in1  : std_logic_vector(8-1 downto 0);
    signal data_out1 : std_logic_vector(8-1 downto 0);
    signal data_in2  : std_logic_vector(8-1 downto 0);
    signal data_out2 : std_logic_vector(8-1 downto 0);
    signal data_in3  : std_logic_vector(8-1 downto 0);
    signal data_out3 : std_logic_vector(8-1 downto 0);
	
	--clk control output signals
	signal int_VL_UserClk : std_logic;
	signal int_VL_UserRst : std_logic;
	
	--BRAM0 control signals
	signal int_BRAM0WDATA 	: std_logic_vector(31 downto 0);
	signal int_BRAM0RDATA 	: std_logic_vector(31 downto 0);
	signal int_BRAM0ADDRESS : std_logic_vector(13 downto 0);
	signal int_BRAM0READ	: std_logic;
	signal int_BRAM0WRITE	: std_logic;
	
	--BRAM1 control signals
	signal int_BRAM1WDATA 	: std_logic_vector(31 downto 0);
	signal int_BRAM1RDATA 	: std_logic_vector(31 downto 0);
	signal int_BRAM1ADDRESS : std_logic_vector(13 downto 0);
	signal int_BRAM1READ	: std_logic;
	signal int_BRAM1WRITE	: std_logic;
	
	--COMMAND SIGNALS
	--clk & reset commands
	alias command_reset 		: std_logic is int_variableInMemory(0);
	alias command_clk_run 		: std_logic is int_variableInMemory(1);
	alias command_clk_step 		: std_logic is int_variableInMemory(2);
	alias command_step_value 	: std_logic_vector is int_variableInMemory(63 downto 32); --32bit step value
	--alias command_clk_hstep 	: std_logic is int_variableInMemory(3);
	--BRAM0 commands
	alias bram0_control_sel 	: std_logic is int_variableInMemory(3);
	alias BRAM0READ_override  	: std_logic is int_variableInMemory(4);
	alias BRAM0WRITE_override  	: std_logic is int_variableInMemory(5);
	--BRAM1 commands
	alias bram1_control_sel 	: std_logic is int_variableInMemory(6);
	alias BRAM1READ_override  	: std_logic is int_variableInMemory(7);
	alias BRAM1WRITE_override  	: std_logic is int_variableInMemory(8);
	
	alias unused0				: std_logic_vector is int_variableInMemory(31 downto 9); --unused registers
	
	--BRAM0 data
	alias BRAM0WDATA_override  	: std_logic_vector is int_variableInMemory(95 downto 64); 	--easier alignment for now
	alias BRAM0ADDRESS_override : std_logic_vector is int_variableInMemory(109 downto 96); 	
	alias BRAM0RDATA_override 	: std_logic_vector is int_variableOutMemory(31 downto 0);
	
	alias unused1				: std_logic_vector is int_variableInMemory(127 downto 110);--unused registers
	
	--BRAM1 data
	alias BRAM1WDATA_override  	: std_logic_vector is int_variableInMemory(159 downto 128); --easier alignment for now
	alias BRAM1ADDRESS_override : std_logic_vector is int_variableInMemory(173 downto 160);
	alias BRAM1RDATA_override 	: std_logic_vector is int_variableOutMemory(63 downto 32);
	
	alias unused2				: std_logic_vector is int_variableInMemory(191 downto 174);--unused registers
	
	-- *********************************************************************************** --
	-- *********************** USERDESIGN MEMORY ASSIGNMENT ****************************** --
	-- *********************************************************************************** --
	-- Only accessible memory by user design is 512 bits of inputs, 4096 bits of outputs
	-- Rest of variableInMemory and variableOutMemory are reserved for commands and peripherals
	
	-- First 192 bits are reserved for commands (clk, rst, step, BRAMs etc.)
	alias USERDESIGN_memory_in	: std_logic_vector is int_variableInMemory(703 downto 192);
	-- First 128 bits are reserved for data out for BRAMs etc
	alias USERDESIGN_memory_out	: std_logic_vector is int_variableOutMemory(4223 downto 128);
	
	
	--------------------------------------------------
	--USER DESIGN CLK and RST CONTROLLER
	component CLK_RST_CONTROL IS
	generic (
		INPUT_CLK_FREQ 	: integer := 100000000; --100MHz PYNQ lowest sys clk
		OUTPUT_CLK_FREQ : integer := 25000000 	--25MHz desired output clk
	);
	port (
		CLK 				: IN STD_LOGIC;
		RST 				: IN STD_LOGIC;
		VL_UserClk 			: OUT Std_logic;
		VL_UserRst 			: OUT Std_logic;
		COMMAND_RST 			: IN Std_logic;
		COMMAND_CLK_FREE_RUN 	: IN Std_logic;
		COMMAND_CLK_STEP 		: IN Std_logic;
		COMMAND_STEP_VALUE		: IN Std_logic_vector(31 downto 0)
		--************************************
		--**Do we need the HALF_STEP CMD????**
		--************************************
		--COMMAND_CLK_HALF_STEP 	: IN STD_LOGIC
	);
	END component;
	
	--16K BRAM IP from Xilinx
	--32bit wide write/read
	--features global enable/disable (ENA)
	--and a write enable (WEA)
	----If WEA is low, assumed to be a read
	component BRAM0 is
		port (
			ADDRA 	: in std_logic_vector(13 downto 0);
			CLKA	: in std_logic;
			DINA	: in std_logic_vector(31 downto 0);
			DOUTA	: out std_logic_vector(31 downto 0);
			ENA		: in std_logic;
			WEA		: in std_logic_vector(0 downto 0)
		);
	END component;
	
begin
	-- I/O Connections assignments

	S_AXI_AWREADY	<= axi_awready;
	S_AXI_WREADY	<= axi_wready;
	S_AXI_BRESP	<= axi_bresp;
	--S_AXI_BUSER	<= axi_buser;
	S_AXI_BVALID	<= axi_bvalid;
	S_AXI_ARREADY	<= axi_arready;
	S_AXI_RDATA	<= axi_rdata;
	S_AXI_RRESP	<= axi_rresp;
	S_AXI_RLAST	<= axi_rlast;
	--S_AXI_RUSER	<= axi_ruser;
	S_AXI_RVALID	<= axi_rvalid;
	S_AXI_BID <= S_AXI_AWID;
	S_AXI_RID <= S_AXI_ARID;
	aw_wrap_size <= ((C_S_AXI_DATA_WIDTH)/8 * to_integer(unsigned(axi_awlen))); 
	ar_wrap_size <= ((C_S_AXI_DATA_WIDTH)/8 * to_integer(unsigned(axi_arlen))); 
	aw_wrap_en <= '1' when (((axi_awaddr AND std_logic_vector(to_unsigned(aw_wrap_size,C_S_AXI_ADDR_WIDTH))) XOR std_logic_vector(to_unsigned(aw_wrap_size,C_S_AXI_ADDR_WIDTH))) = low) else '0';
	ar_wrap_en <= '1' when (((axi_araddr AND std_logic_vector(to_unsigned(ar_wrap_size,C_S_AXI_ADDR_WIDTH))) XOR std_logic_vector(to_unsigned(ar_wrap_size,C_S_AXI_ADDR_WIDTH))) = low) else '0';
	--S_AXI_BUSER <= (others => '0');

	-- Implement axi_awready generation

	-- axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	-- de-asserted when reset is low.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_awready <= '0';
	      axi_awv_awr_flag <= '0';
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and axi_awv_awr_flag = '0' and axi_arv_arr_flag = '0') then
	        -- slave is ready to accept an address and
	        -- associated control signals
	        axi_awv_awr_flag  <= '1'; -- used for generation of bresp() and bvalid
	        axi_awready <= '1';
	      elsif (S_AXI_WLAST = '1' and axi_wready = '1') then 
	      -- preparing to accept next address after current write burst tx completion
	        axi_awv_awr_flag  <= '0';
	      else
	        axi_awready <= '0';
	      end if;
	    end if;
	  end if;         
	end process; 
	-- Implement axi_awaddr latching

	-- This process is used to latch the address when both 
	-- S_AXI_AWVALID and S_AXI_WVALID are valid. 

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_awaddr <= (others => '0');
	      axi_awburst <= (others => '0'); 
	      axi_awlen <= (others => '0'); 
	      axi_awlen_cntr <= (others => '0');
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and axi_awv_awr_flag = '0') then
	      -- address latching 
	        axi_awaddr <= S_AXI_AWADDR(C_S_AXI_ADDR_WIDTH - 1 downto 0);  ---- start address of transfer
	        axi_awlen_cntr <= (others => '0');
	        axi_awburst <= S_AXI_AWBURST;
	        axi_awlen <= S_AXI_AWLEN;
	      elsif((axi_awlen_cntr <= axi_awlen) and axi_wready = '1' and S_AXI_WVALID = '1') then     
	        axi_awlen_cntr <= std_logic_vector (unsigned(axi_awlen_cntr) + 1);

	        case (axi_awburst) is
	          when "00" => -- fixed burst
	            -- The write address for all the beats in the transaction are fixed
	            axi_awaddr     <= axi_awaddr;       ----for awsize = 4 bytes (010)
	          when "01" => --incremental burst
	            -- The write address for all the beats in the transaction are increments by awsize
	            axi_awaddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB) <= std_logic_vector (unsigned(axi_awaddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB)) + 1);--awaddr aligned to 4 byte boundary
	            axi_awaddr(ADDR_LSB-1 downto 0)  <= (others => '0');  ----for awsize = 4 bytes (010)
	          when "10" => --Wrapping burst
	            -- The write address wraps when the address reaches wrap boundary 
	            if (aw_wrap_en = '1') then
	              axi_awaddr <= std_logic_vector (unsigned(axi_awaddr) - (to_unsigned(aw_wrap_size,C_S_AXI_ADDR_WIDTH)));                
	            else 
	              axi_awaddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB) <= std_logic_vector (unsigned(axi_awaddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB)) + 1);--awaddr aligned to 4 byte boundary
	              axi_awaddr(ADDR_LSB-1 downto 0)  <= (others => '0');  ----for awsize = 4 bytes (010)
	            end if;
	          when others => --reserved (incremental burst for example)
	            axi_awaddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB) <= std_logic_vector (unsigned(axi_awaddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB)) + 1);--for awsize = 4 bytes (010)
	            axi_awaddr(ADDR_LSB-1 downto 0)  <= (others => '0');
	        end case;        
	      end if;
	    end if;
	  end if;
	end process;
	-- Implement axi_wready generation

	-- axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	-- de-asserted when reset is low. 

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_wready <= '0';
	    else
	      if (axi_wready = '0' and S_AXI_WVALID = '1' and axi_awv_awr_flag = '1') then
	        axi_wready <= '1';
	        -- elsif (axi_awv_awr_flag = '0') then
	      elsif (S_AXI_WLAST = '1' and axi_wready = '1') then 

	        axi_wready <= '0';
	      end if;
	    end if;
	  end if;         
	end process; 
	-- Implement write response logic generation

	-- The write response and response valid signals are asserted by the slave 
	-- when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	-- This marks the acceptance of address and indicates the status of 
	-- write transaction.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_bvalid  <= '0';
	      axi_bresp  <= "00"; --need to work more on the responses
	    else
	      if (axi_awv_awr_flag = '1' and axi_wready = '1' and S_AXI_WVALID = '1' and axi_bvalid = '0' and S_AXI_WLAST = '1' ) then
	        axi_bvalid <= '1';
	        axi_bresp  <= "00"; 
	      elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then  
	      --check if bready is asserted while bvalid is high)
	        axi_bvalid <= '0';                      
	      end if;
	    end if;
	  end if;         
	end process; 
	-- Implement axi_arready generation

	-- axi_arready is asserted for one S_AXI_ACLK clock cycle when
	-- S_AXI_ARVALID is asserted. axi_awready is 
	-- de-asserted when reset (active low) is asserted. 
	-- The read address is also latched when S_AXI_ARVALID is 
	-- asserted. axi_araddr is reset to zero on reset assertion.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_arready <= '0';
	      axi_arv_arr_flag <= '0';
	    else
	      if (axi_arready = '0' and S_AXI_ARVALID = '1' and axi_awv_awr_flag = '0' and axi_arv_arr_flag = '0') then
	        axi_arready <= '1';
	        axi_arv_arr_flag <= '1';
	      elsif (axi_rvalid = '1' and S_AXI_RREADY = '1' and (axi_arlen_cntr = axi_arlen)) then 
	      -- preparing to accept next address after current read completion
	        axi_arv_arr_flag <= '0';
	      else
	        axi_arready <= '0';
	      end if;
	    end if;
	  end if;         
	end process; 
	-- Implement axi_araddr latching

	--This process is used to latch the address when both 
	--S_AXI_ARVALID and S_AXI_RVALID are valid. 
	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_araddr <= (others => '0');
	      axi_arburst <= (others => '0');
	      axi_arlen <= (others => '0'); 
	      axi_arlen_cntr <= (others => '0');
	      axi_rlast <= '0';
	    else
	      if (axi_arready = '0' and S_AXI_ARVALID = '1' and axi_arv_arr_flag = '0') then
	        -- address latching 
	        axi_araddr <= S_AXI_ARADDR(C_S_AXI_ADDR_WIDTH - 1 downto 0); ---- start address of transfer
	        axi_arlen_cntr <= (others => '0');
	        axi_rlast <= '0';
	        axi_arburst <= S_AXI_ARBURST;
	        axi_arlen <= S_AXI_ARLEN;
	      elsif((axi_arlen_cntr <= axi_arlen) and axi_rvalid = '1' and S_AXI_RREADY = '1') then     
	        axi_arlen_cntr <= std_logic_vector (unsigned(axi_arlen_cntr) + 1);
	        axi_rlast <= '0';      
	     
	        case (axi_arburst) is
	          when "00" =>  -- fixed burst
	            -- The read address for all the beats in the transaction are fixed
	            axi_araddr     <= axi_araddr;      ----for arsize = 4 bytes (010)
	          when "01" =>  --incremental burst
	            -- The read address for all the beats in the transaction are increments by awsize
	            axi_araddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB) <= std_logic_vector (unsigned(axi_araddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB)) + 1); --araddr aligned to 4 byte boundary
	            axi_araddr(ADDR_LSB-1 downto 0)  <= (others => '0');  ----for awsize = 4 bytes (010)
	          when "10" =>  --Wrapping burst
	            -- The read address wraps when the address reaches wrap boundary 
	            if (ar_wrap_en = '1') then   
	              axi_araddr <= std_logic_vector (unsigned(axi_araddr) - (to_unsigned(ar_wrap_size,C_S_AXI_ADDR_WIDTH)));
	            else 
	              axi_araddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB) <= std_logic_vector (unsigned(axi_araddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB)) + 1); --araddr aligned to 4 byte boundary
	              axi_araddr(ADDR_LSB-1 downto 0)  <= (others => '0');  ----for awsize = 4 bytes (010)
	            end if;
	          when others => --reserved (incremental burst for example)
	            axi_araddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB) <= std_logic_vector (unsigned(axi_araddr(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB)) + 1);--for arsize = 4 bytes (010)
			  axi_araddr(ADDR_LSB-1 downto 0)  <= (others => '0');
	        end case;         
	      elsif((axi_arlen_cntr = axi_arlen) and axi_rlast = '0' and axi_arv_arr_flag = '1') then  
	        axi_rlast <= '1';
	      elsif (S_AXI_RREADY = '1') then  
	        axi_rlast <= '0';
	      end if;
	    end if;
	  end if;
	end  process;  
	-- Implement axi_arvalid generation

	-- axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	-- S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	-- data are available on the axi_rdata bus at this instance. The 
	-- assertion of axi_rvalid marks the validity of read data on the 
	-- bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	-- is deasserted on reset (active low). axi_rresp and axi_rdata are 
	-- cleared to zero on reset (active low).  

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_rvalid <= '0';
	      axi_rresp  <= "00";
	    else
	      if (axi_arv_arr_flag = '1' and axi_rvalid = '0') then
	        axi_rvalid <= '1';
	        axi_rresp  <= "00"; -- 'OKAY' response
	      elsif (axi_rvalid = '1' and S_AXI_RREADY = '1') then
	        axi_rvalid <= '0';
	      end  if;      
	    end if;
	  end if;
	end  process;
	
	-- ------------------------------------------
	-- -- Example code to access user logic memory region
	-- ------------------------------------------
	--ADDR_LSB = 2
	--OPT_MEM_ADDR_BITS = 8
	mem_address <= axi_araddr(ADDR_LSB+OPT_MEM_ADDR_BITS downto ADDR_LSB) when axi_arv_arr_flag = '1' else
	                 axi_awaddr(ADDR_LSB+OPT_MEM_ADDR_BITS downto ADDR_LSB) when axi_awv_awr_flag = '1' else
	                 (others => '0');
	
	-- implement Block RAM(s)
	mem_wren <= axi_wready and S_AXI_WVALID ;
	mem_rden <= axi_arv_arr_flag ;
	
	--assigning 8 bit data into Byte signals
	data_in0  <= S_AXI_WDATA((7) downto 0);
	data_in1  <= S_AXI_WDATA((15) downto 8);
	data_in2  <= S_AXI_WDATA((23) downto 16);
	data_in3  <= S_AXI_WDATA((31) downto 24);
		
	--MEMORY READ (SPLIT INTO BYTE READS)
	process(mem_address, int_variableInMemory, int_variableOutMemory) 
	begin
	--32 words = 1024bits of addressable input memory
	--132 words = 4224bits of addressable output memory
	--mem_address goes from 0 to 32 for inputs and 32 to 164 for outputs
	if (to_integer(unsigned(mem_address)) < 32) then 
		data_out0 <= int_variableInMemory((to_integer(unsigned(mem_address))*32)+7 downto to_integer(unsigned(mem_address))*32);
		data_out1 <= int_variableInMemory((to_integer(unsigned(mem_address))*32)+7+8 downto (to_integer(unsigned(mem_address))*32)+8);
		data_out2 <= int_variableInMemory((to_integer(unsigned(mem_address))*32)+7+16 downto (to_integer(unsigned(mem_address))*32)+16);
		data_out3 <= int_variableInMemory((to_integer(unsigned(mem_address))*32)+7+24 downto (to_integer(unsigned(mem_address))*32)+24);
	elsif (to_integer(unsigned(mem_address)) < 164) then -- -1024 because address (32 * 32 = 1024) so need to offset for that
		data_out0 <= int_variableOutMemory((to_integer(unsigned(mem_address))*32)+7-1024 downto (to_integer(unsigned(mem_address))*32)-1024);
		data_out1 <= int_variableOutMemory((to_integer(unsigned(mem_address))*32)+7-1024+8 downto (to_integer(unsigned(mem_address))*32)-1024+8);
		data_out2 <= int_variableOutMemory((to_integer(unsigned(mem_address))*32)+7-1024+16 downto (to_integer(unsigned(mem_address))*32)-1024+16);
		data_out3 <= int_variableOutMemory((to_integer(unsigned(mem_address))*32)+7-1024+24 downto (to_integer(unsigned(mem_address))*32)-1024+24);
	else -- addresses > 164 are invalid: return all 0's
		data_out0 <= (others => '0');
		data_out1 <= (others => '0');
		data_out2 <= (others => '0');
		data_out3 <= (others => '0');
	end if;
	end process;
	   
	--INPUT MEMORY WRITE (SPLIT INTO BYTE WRITES)
	BYTE_RAM_PROC : process( S_AXI_ACLK ) is
	begin
	if ( rising_edge (S_AXI_ACLK) ) then
		if ( mem_wren = '1' and S_AXI_WSTRB(0) = '1' and to_integer(unsigned(mem_address)) < 32 ) then
			int_variableInMemory((to_integer(unsigned(mem_address))*32)+7 downto to_integer(unsigned(mem_address))*32) <= data_in0;
		end if;
		if ( mem_wren = '1' and S_AXI_WSTRB(1) = '1' and to_integer(unsigned(mem_address)) < 32 ) then
			int_variableInMemory((to_integer(unsigned(mem_address))*32)+7+8 downto (to_integer(unsigned(mem_address))*32)+8) <= data_in1;
		end if;
		if ( mem_wren = '1' and S_AXI_WSTRB(2) = '1' and to_integer(unsigned(mem_address)) < 32 ) then
			int_variableInMemory((to_integer(unsigned(mem_address))*32)+7+16 downto (to_integer(unsigned(mem_address))*32)+16) <= data_in2;
		end if;
		if ( mem_wren = '1' and S_AXI_WSTRB(3) = '1' and to_integer(unsigned(mem_address)) < 32 ) then
			int_variableInMemory((to_integer(unsigned(mem_address))*32)+7+24  downto (to_integer(unsigned(mem_address))*32)+24) <= data_in3;
		end if;
	end if;
	end process BYTE_RAM_PROC;
	   
	   
	process( S_AXI_ACLK ) is
	begin
	if ( rising_edge (S_AXI_ACLK) ) then
		if ( mem_rden = '1') then 
			mem_data_out((7) downto 0) <= data_out0;
			mem_data_out((15) downto 8) <= data_out1;
			mem_data_out((23) downto 16) <= data_out2;
			mem_data_out((31) downto 24) <= data_out3;
		end if;
	end if;
	end process;
	 
	--Output register or memory read data
	process(mem_data_out, axi_rvalid ) is
	begin
	if (axi_rvalid = '1') then
		-- When there is a valid read address (S_AXI_ARVALID) with 
		-- acceptance of read address by the slave (axi_arready), 
		-- output the read dada 
		axi_rdata <= mem_data_out;  -- memory range 0 read data
	else
		axi_rdata <= (others => '0');
	end if;  
	end process;
	----------------------END AXI SIGNAL HANDLING--------------------------------------------------
	
	variableInMemory <= USERDESIGN_memory_in;
	USERDESIGN_memory_out <= variableOutMemory;
	
	clkControlFSM_i: CLK_RST_CONTROL port map(
		CLK 				=> S_AXI_ACLK,
        RST 				=> (not S_AXI_ARESETN),
        VL_UserClk 			=> VL_UserClk,
        VL_UserRst 			=> VL_UserRst,
        COMMAND_RST 			=> command_reset,
        COMMAND_CLK_FREE_RUN 	=> command_clk_run,
        COMMAND_CLK_STEP 		=> command_clk_step,
		COMMAND_STEP_VALUE		=> command_step_value
        --COMMAND_CLK_HALF_STEP 	=> command_clk_hstep
	);
	
	BRAM_controller_sel: process(int_variableInMemory, int_BRAM0RDATA, BRAM0ADDRESS, BRAM0WDATA, BRAM0READ, 
														int_BRAM1RDATA, BRAM1ADDRESS, BRAM1WDATA, BRAM1READ)
	begin
	-- BRAM0
	if bram0_control_sel = '1' then -- controller (i.e. ARM) takes control of BRAM0
		int_BRAM0ADDRESS <= BRAM0ADDRESS_override;
		int_BRAM0WDATA 	<= BRAM0WDATA_override; 
		int_BRAM0READ 	<= BRAM0READ_override;
		int_BRAM0WRITE 	<= BRAM0WRITE_override;
		BRAM0RDATA_override <= int_BRAM0RDATA;
	else 							-- user design (i.e. DLM_Userlogic) takes control of BRAM0
		int_BRAM0ADDRESS <= BRAM0ADDRESS;
		int_BRAM0WDATA 	<= BRAM0WDATA; 
		int_BRAM0READ 	<= BRAM0READ;
		int_BRAM0WRITE 	<= BRAM0WRITE;
		BRAM0RDATA <= int_BRAM0RDATA;
	end if;
	
	-- BRAM1
	if bram1_control_sel = '1' then -- controller (i.e. ARM) takes control of BRAM1
		int_BRAM1ADDRESS <= BRAM1ADDRESS_override;
		int_BRAM1WDATA 	<= BRAM1WDATA_override; 
		int_BRAM1READ 	<= BRAM1READ_override;
		int_BRAM1WRITE 	<= BRAM1WRITE_override;
		BRAM1RDATA_override <= int_BRAM1RDATA;
	else 							-- user design (i.e. DLM_Userlogic) takes control of BRAM1
		int_BRAM1ADDRESS <= BRAM1ADDRESS;
		int_BRAM1WDATA 	<= BRAM1WDATA; 
		int_BRAM1READ 	<= BRAM1READ;
		int_BRAM1WRITE 	<= BRAM1WRITE;
		BRAM1RDATA <= int_BRAM1RDATA;
	end if;
	end process;
		
	--I/O devices
	SWITCHES <= EXTSWITCHES;
	BUTTONS <= EXTBUTTONS;
	EXTDISPLAYLED <= DISPLAYLED;
	EXTRGBLED <= RGBLED;
	
	BRAM0_inst: BRAM0 port map(
			ADDRA 	=> int_BRAM0ADDRESS,
			CLKA	=> S_AXI_ACLK,
			DINA	=> int_BRAM0WDATA,
			DOUTA	=> int_BRAM0RDATA,
			ENA		=> (int_BRAM0READ or int_BRAM0WRITE),
			WEA(0)	=> int_BRAM0WRITE
		);
		
	BRAM1_inst: BRAM0 port map(
			ADDRA 	=> int_BRAM1ADDRESS,
			CLKA	=> S_AXI_ACLK,
			DINA	=> int_BRAM1WDATA,
			DOUTA	=> int_BRAM1RDATA,
			ENA		=> (int_BRAM1READ or int_BRAM1WRITE),
			WEA(0)	=> int_BRAM1WRITE
		);
	
end arch_imp;
