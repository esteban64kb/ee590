library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CLK_RST_CONTROL IS
	generic (
		INPUT_CLK_FREQ 	: integer := 100000000; --100MHz PYNQ lowest sys clk
		OUTPUT_CLK_FREQ : integer := 25000000 	--25MHz desired output clk
	);
	port (
		clk 				: IN STD_LOGIC;
		rst 				: IN STD_LOGIC;
		VL_UserClk 			: OUT Std_logic;
		VL_UserRst 			: OUT Std_logic;
		COMMAND_RST 			: IN Std_logic;
		COMMAND_CLK_FREE_RUN 	: IN Std_logic;
		COMMAND_CLK_STEP 		: IN Std_logic;
		COMMAND_STEP_VALUE		: IN Std_logic_vector(31 downto 0)
	);
	END CLK_RST_CONTROL;

architecture RTL of CLK_RST_CONTROL is
type stateType is (idle, stepState1, stepState0, stepStateFinish); 
signal CS, NS: stateType;
signal toggleClk	 	    : std_logic;
signal intClk   	 	    : std_logic := '0';

--clk step counters
signal CS_step	: std_logic_vector(31 downto 0);
signal NS_step	: std_logic_vector(31 downto 0);

--clk divider signals
constant maxValue : integer := (INPUT_CLK_FREQ/OUTPUT_CLK_FREQ);
signal CS_count : integer range 0 to maxValue-1 := 0;
signal NS_count : integer range 0 to maxValue-1 := 0;


BEGIN

asgnVL_UserRst_i: VL_UserRst <= rst or COMMAND_RST;

clkReg_i: process (clk)
begin
	if clk'event and clk = '1' then
		if toggleClk = '1' then
			intClk <= not intClk;
		end if;
	end if;
end process;
asgnVL_UserClk_i: VL_UserClk <= intClk;


stateReg_i: process (clk, rst)
begin
if rst = '1' then 
	CS_count <= 0;
	CS_step <= (others => '0');
	CS <= idle;	
elsif clk'event and clk = '1' then 
	CS_count <= NS_count;
	CS_step <= NS_step;
	CS <= NS;
end if;
end process; 


NSDecode_i: process (CS, CS_count, COMMAND_CLK_FREE_RUN, COMMAND_CLK_STEP, COMMAND_STEP_VALUE) 
begin
   toggleClk <= '0';  -- default assignments
   NS_count <= CS_count;
   NS_step <= CS_step;
   NS <= CS; 				
   case CS is

		when idle => 		
			if COMMAND_CLK_FREE_RUN = '1' then 
				if CS_count = maxValue-1 then --clk divider to limit free run clk speed to 25MHz (default)
					NS_count <= 0;
					toggleClk <= '1';          -- synchronously toggle intClk on next clk active edge 
				else
					NS_count <= CS_count + 1;
				end if;
			elsif COMMAND_CLK_STEP = '1' then 
				NS_step <= COMMAND_STEP_VALUE;
				NS <= stepState1;  
				
			end if;

		when stepState1 =>  			   -- high to low part of clk step
				if CS_step = X"00000000" then
					NS <= stepStateFinish;
				else
					NS_step <= std_logic_vector(unsigned(CS_step) - 1);
					NS <= stepState0;
					toggleClk <= '1'; -- synchronously toggle intClk 1 to 0 on next clk active edge 
				end if;
		
		when stepState0 => 
			--must have toggleclk high for 2 clk periods
			--before decrementing step counter
			toggleClk <= '1';
			NS <= stepState1;
		
		when stepStateFinish => 
			if COMMAND_CLK_STEP = '0' then -- wait for command deassertion
				NS <= idle; 
			else 
				NS <= stepStateFinish;
			end if;

		when others => 
			null;  
   end case;
end process; 
end RTL;