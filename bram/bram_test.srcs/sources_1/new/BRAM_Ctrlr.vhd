----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Esteban Moreno
-- 
-- Create Date: 03/04/2019 03:49:10 PM
-- Design Name: 
-- Module Name: BRAM_Ctrlr - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity BRAM_Ctrlr is
    Port ( 
            clk 		: in STD_LOGIC;
            rst         : in STD_LOGIC;
            goBRAM      : in STD_LOGIC;
            extBRAMAdd  : in STD_LOGIC_VECTOR(11 downto 0);
            extBRAMDat  : in STD_LOGIC_VECTOR(15 downto 0);
            extBRAMAccess   : in STD_LOGIC;
            extBRAMEna  : in STD_LOGIC;
            BRAMDOut    : out STD_LOGIC_VECTOR(15 downto 0)
    );
end BRAM_Ctrlr;

architecture Behavioral of BRAM_Ctrlr is

type stateType is (idle, waitWrDone); -- complete state enumeration
signal CS, NS    : stateType;

signal ld0          : std_logic;
signal TC           : std_logic;
signal FSMBRAMWea    : STD_LOGIC_VECTOR(0 DOWNTO 0);
signal FSMBRAMEna    : std_logic;
signal FSMBRAMAddr       : std_logic_vector(11 downto 0);
signal FSMBRAMDat       : std_logic_vector(15 downto 0);

component bram IS
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END component;

begin

bram_i: bram PORT MAP ( clka => clk,
              ena => FSMBRAMEna,
              wea => FSMBRAMWea,
              addra => FSMBRAMAddr,
              dina => FSMBRAMDat,
              douta => BRAMDOut
              );

stateReg_i: process(clk, rst)
begin
    if rst = '1' then
        CS <= idle;
    elsif clk'event and clk = '1' then
        CS <= NS;
    end if;
end process;

NSAndOPDecode_i: process(CS, goBRAM, TC)
begin
 NS      <= CS; -- default  

 case CS is     	 
	when idle =>   
      if goBRAM = '1' then
         ld0 <= '1'; 
         NS      <= waitWrDone;
      end if;
	
	when waitWrDone =>
	       ld0 <= '0';
	       FSMBRAMWea(0) <= '1';
           FSMBRAMEna <= '1';
           
           if TC = '1' then
            NS <= idle;
                FSMBRAMWea(0) <= '0';
                --FSMBRAMEna <= '0';
           end if;

    when others => 
	    null;
 end case; 
end process;

Count4k_i: process (clk, ld0)
begin
   if ld0 = '1' then
      FSMBRAMAddr <= (others => '0');
   elsif clk'event and clk = '1' then
      if ld0 = '0' and TC = '0' then                                         
         FSMBRAMAddr <= std_logic_vector(unsigned(FSMBRAMAddr) + 1);
      end if;
   end if;
end process;

TC_i: TC <= '1' when FSMBRAMAddr >= "111111111111" else '0';

FSMBRAMDat_i: FSMBRAMDat <= extBRAMDat;

--extBRAMAccess_i: FSMBRAMAddr <= extBRAMAdd when extBRAMAccess = '1';

--extBRAMAccess_i: process(extBRAMAccess)
--begin
--    if extBRAMAccess = '1' then
--        FSMBRAMAddr <= extBRAMAdd;
--   elsif extBRAMAccess = '0' then
--        FSMBRAMAddr <= (others => '0');
--    end if;
--end process;

end Behavioral;
