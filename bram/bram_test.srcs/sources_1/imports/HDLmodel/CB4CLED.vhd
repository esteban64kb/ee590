-- Description: CB4CLED 4-bit cascadable up/down, loadable counter with chip enable, asynchronous rst
-- Outputs: count(3:0), terminal count (TC) and clock enable out (ceo)
-- Engineer: Fearghal Morgan
-- viciLogic 
-- Date: 7/12/2012
-- Change History: Initial version
-- 
-- clk			System clock strobe, rising edge active
-- rst			Synchronous reset signal. Assertion clears all registers, count=00
-- loadDat(3:0)	4-bit load data value
-- load			Assertion (H) synchronously loads count(3:0) register with loadDat(3:0) 
--              Load function does not rwquire assertion of signal ce
-- ce			Assertion (H) enable synchronous count behaviour 
-- up			Assertion (H) / deassertion (L) enables count up/down behaviour
-- count(3:0)	Counter value, changes synchronously on active (rising) clk edge
-- TC			Asserted (H) when up counter   (up=1) and count=Fh 
-- 							 when down counter (up=0) and count=0
-- ceo			Asserted (H) when both ce and TC are asserted 

-- countIndex
-- up, ce, load Index
-- CS + 1
-- CS - 1
-- intTC

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Count4K is
    Port ( clk 		: in STD_LOGIC;
           ld0      : in STD_LOGIC;
           rst 		: in STD_LOGIC;   
           loadDat	: in STD_LOGIC_VECTOR (3 downto 0); 
           load		: in STD_LOGIC;                
           ce 		: in STD_LOGIC;                
           up 		: in STD_LOGIC;                
           count	: out STD_LOGIC_VECTOR (3 downto 0);
           TC		: out STD_LOGIC;                    
           ceo		: out STD_LOGIC                     
           );
end Count4K;

architecture RTL of Count4K is
signal NS : STD_LOGIC_VECTOR(3 downto 0); -- next state
signal CS : STD_LOGIC_VECTOR(3 downto 0); -- current state  
signal intTC : STD_LOGIC;                 -- internal signal (same as TC)

begin

NSDecode_i: process(CS, loadDat, load, ce, up)
	begin
		NS <= CS; -- default assignment
		if load = '1' then
			NS <= loadDat;
		elsif ce = '1' then
			if up = '1' then NS <= std_logic_vector(unsigned(CS) + 1);
			else     	 	 NS <= std_logic_vector(unsigned(CS) - 1);
            end if;
		end if;
end process;

OPDecode_intTC_i: process (up, CS)
begin
	intTC <= '0'; -- default
	if    up = '1' and CS = "1111" then  
	   intTC <= '1';   
    elsif up = '0' and CS = "0000" then  
       intTC <= '1';   
    end if;
end process;
OPDecode_TC_i:  TC  <= intTC; 
OPDecode_ceo_i: ceo <= '1' when (intTC = '1' and ce = '1') else '0';  



stateReg_i: process(clk, ld0)
begin
	if ld0 = '1' then           		
        CS <= (others => '0');
	elsif clk'event and clk = '1' then	
        CS <= NS;
	end if;
end process;

asgnCount_i: count <= CS; 

end RTL;