-- Description: Count4k
-- Engineer: Esteban Moreno

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Count4K is
    Port ( clk 		: in STD_LOGIC;
           ld0      : in STD_LOGIC;
           --rst 		: in STD_LOGIC;                
           count	: out STD_LOGIC_VECTOR (11 downto 0);
           TC       : out STD_LOGIC              
           );
end Count4K;

architecture RTL of Count4K is
signal NS : STD_LOGIC_VECTOR(11 downto 0); -- next state
signal CS : STD_LOGIC_VECTOR(11 downto 0); -- current state  
signal intTC : STD_LOGIC;                 -- internal signal (same as TC)

begin

NSDecode_i: process(CS)
	begin
		NS <= CS; -- default assignment
		
		if intTC = '0' then    -- Only increment when TC deasserted
		  NS <= std_logic_vector(unsigned(CS) + 1);
		end if;
end process;

OPDecode_intTC_i: process (CS)
begin
	intTC <= '0'; -- default
	if CS = "111111111110" then   -- Stop a number earlier
	   intTC <= '1';     
    end if;
end process;

OPDecode_TC_i: TC <= intTC; 

stateReg_i: process(clk, ld0)
begin
	if ld0 = '1' then           		
        CS <= (others => '0');
	elsif clk'event and clk = '1' then	
        CS <= NS;
	end if;
end process;

asgnCount_i: count <= CS; 

end RTL;