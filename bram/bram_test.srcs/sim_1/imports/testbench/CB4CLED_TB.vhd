-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 3.3.2019 22:24:26 GMT

library ieee;
use ieee.std_logic_1164.all;

entity tb_Count4K is
end tb_Count4K;

architecture tb of tb_Count4K is

    component Count4K
        port (clk     : in std_logic;
              rst     : in std_logic;
              ld0      : in STD_LOGIC;
              loadDat : in std_logic_vector (3 downto 0);
              load    : in std_logic;
              ce      : in std_logic;
              up      : in std_logic;
              count   : out std_logic_vector (3 downto 0);
              TC      : out std_logic;
              ceo     : out std_logic);
    end component;

    signal clk     : std_logic;
    signal rst     : std_logic;
    signal ld0     : std_logic;
    signal loadDat : std_logic_vector (3 downto 0);
    signal load    : std_logic;
    signal ce      : std_logic;
    signal up      : std_logic;
    signal count   : std_logic_vector (3 downto 0);
    signal TC      : std_logic;
    signal ceo     : std_logic;

    constant TbPeriod : time := 20 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : Count4K
    port map (clk     => clk,
              rst     => rst,
              ld0     => ld0,
              loadDat => loadDat,
              load    => load,
              ce      => ce,
              up      => up,
              count   => count,
              TC      => TC,
              ceo     => ceo);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        loadDat <= (others => '0');
        load <= '0';
        ce <= '0';
        up <= '0';

        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        rst <= '1';
        ld0 <= '1';
        wait for TbPeriod;
        rst <= '0';
        ld0 <= '0';
        ce <= '1';
        up <= '1';
        wait for TBPeriod;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;