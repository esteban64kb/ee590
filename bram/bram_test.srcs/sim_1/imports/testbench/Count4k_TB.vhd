-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 3.3.2019 22:24:26 GMT

library ieee;
use ieee.std_logic_1164.all;

entity tb_Count4K is
end tb_Count4K;

architecture tb of tb_Count4K is

    component Count4K
        port(  clk 		: in STD_LOGIC;
               ld0      : in STD_LOGIC;              
               count    : out STD_LOGIC_VECTOR (11 downto 0);
               TC       : out STD_LOGIC
              );
    end component;

    signal clk     : std_logic;
    --signal rst     : std_logic;
    signal ld0     : std_logic;
    signal count   : std_logic_vector (11 downto 0);
    signal TC      : std_logic;
    
    constant TbPeriod : time := 20 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : Count4K
    port map (clk     => clk,
              ld0     => ld0,
              count   => count,
              TC      => TC
              );

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        --rst <= '1';
        ld0 <= '1';
        wait for TbPeriod;
        --rst <= '0';
        ld0 <= '0';

        wait for TBPeriod;

        -- EDIT Add stimuli here
        wait for 10000 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;