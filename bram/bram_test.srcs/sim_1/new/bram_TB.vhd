-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 28.2.2019 12:00:04 GMT

library ieee;
use ieee.std_logic_1164.all;

entity tb_bram is
end tb_bram;

architecture tb of tb_bram is

    component bram
        port (clka  : in std_logic;
              ena   : in std_logic;
              wea   : in std_logic_vector (0 downto 0);
              addra : in std_logic_vector (11 downto 0);
              dina  : in std_logic_vector (15 downto 0);
              douta : out std_logic_vector (15 downto 0));
    end component;

    signal clka  : std_logic;
    signal ena   : std_logic;
    signal wea   : std_logic_vector (0 downto 0);
    signal addra : std_logic_vector (11 downto 0);
    signal dina  : std_logic_vector (15 downto 0);
    signal douta : std_logic_vector (15 downto 0);

    constant TbPeriod : time := 100 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : bram
    port map (clka  => clka,
              ena   => ena,
              wea   => wea,
              addra => addra,
              dina  => dina,
              douta => douta);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clka is really your main clock signal
    clka <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        ena <= '0';
        wea <= (others => '0');
        addra <= (others => '0');
        dina <= (others => '0');

        -- Reset generation
        --  EDIT: Replace YOURRESETSIGNAL below by the name of your reset as I haven't guessed it
        
        --YOURRESETSIGNAL <= '1';
        --wait for 100 ns;
        --YOURRESETSIGNAL <= '0';
        --wait for 100 ns;
        
        wait for 5 * tbPeriod;
        
        ena <= '1';
        dina <= x"1234";
        wea <= (others => '1');
        
        wait for tbPeriod;
        
        wea <= (others => '0');
        addra <= x"001";
        
        wait for TbPeriod;
        
        addra <= x"000";

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_bram of tb_bram is
    for tb
    end for;
end cfg_tb_bram;