-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 4.3.2019 16:29:27 GMT

library ieee;
use ieee.std_logic_1164.all;

entity tb_BRAM_Ctrlr is
end tb_BRAM_Ctrlr;

architecture tb of tb_BRAM_Ctrlr is

    component BRAM_Ctrlr
        port (clk           : in std_logic;
              rst           : in std_logic;
              goBRAM        : in std_logic;
              extBRAMAdd    : in std_logic_vector (11 downto 0);
              extBRAMDat    : in std_logic_vector (15 downto 0);
              extBRAMAccess : in std_logic;
              extBRAMEna    : in std_logic;
              BRAMDOut      : out std_logic_vector (15 downto 0));
    end component;

    signal clk           : std_logic;
    signal rst           : std_logic;
    signal goBRAM        : std_logic;
    signal extBRAMAdd    : std_logic_vector (11 downto 0);
    signal extBRAMDat    : std_logic_vector (15 downto 0);
    signal extBRAMAccess : std_logic;
    signal extBRAMEna    : std_logic;
    signal BRAMDOut      : std_logic_vector (15 downto 0);

    constant TbPeriod : time := 20 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : BRAM_Ctrlr
    port map (clk           => clk,
              rst           => rst,
              goBRAM        => goBRAM,
              extBRAMAdd    => extBRAMAdd,
              extBRAMDat    => extBRAMDat,
              extBRAMAccess => extBRAMAccess,
              extBRAMEna    => extBRAMEna,
              BRAMDOut      => BRAMDOut);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        goBRAM <= '0';
        extBRAMAdd <= (others => '0');
        extBRAMDat <= x"1234";
        extBRAMAccess <= '0';
        extBRAMEna <= '1';

        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        rst <= '1';
        wait for TbPeriod;
        rst <= '0';
        wait for TbPeriod;
        
        goBRAM <= '1';
        
        wait for TbPeriod;
        
        goBRAM <= '0';

        -- EDIT Add stimuli here
        wait for 4100 * TbPeriod;
        
        extBRAMAccess <= '1';
        extBRAMAdd <= x"e22";
        
         wait for 2 * TbPeriod;
         
         extBRAMAccess <= '1';
         extBRAMAdd <= x"fff";
         
          wait for 2 * TbPeriod;
        --extBRAMDat <= x"1234";        

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;